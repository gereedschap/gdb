Run using for example:

```shell
docker run --rm -ti --cap-add=SYS_PTRACE --security-opt seccomp=unconfined -v "$PWD":/pwd --workdir /pwd registry.gitlab.com/gereedschap/gdb tmux new gdb -q 
```

Or use an alias function in your `.<shell>rc`:

```shell
gdb () { docker run --rm -ti --cap-add=SYS_PTRACE --security-opt seccomp=unconfined -v "$PWD":/pwd --workdir /pwd registry.gitlab.com/gereedschap/gdb tmux new gdb -q "$@"; }
```

You can then use the regular tmux bindings, such as `Ctrl-b <arrow-key>` to move panes
