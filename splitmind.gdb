python
import splitmind
(splitmind.Mind()
  .above(display="backtrace")
  .right(display="stack")
  .right(display="regs")
  .right(of="main", display="disasm")
  .right(display="ghidra")
  .left(of="backtrace", cmd="printf 'tty ' > /ttyfd.gdb && tty >> /ttyfd.gdb && tail -f /dev/null", clearing=False)
  .show("legend", on="disasm")
).build()
end
