FROM ubuntu

ARG DEBIAN_FRONTEND=noninteractive

ENV LC_ALL=C.UTF-8

RUN dpkg --add-architecture i386 && apt-get update && apt-get install -y \
build-essential git gdb cmake bison flex libzip-dev python3-dev python3-pip pkg-config sudo tmux vim

RUN pip3 install gdbgui r2pipe

RUN git clone https://github.com/radareorg/radare2.git && \
radare2/sys/install.sh

RUN r2pm init && \
r2pm update && \
r2pm install r2ghidra-dec

RUN git clone https://github.com/pwndbg/pwndbg && \
cd pwndbg && \
./setup.sh

RUN git clone https://github.com/jerdna-regeiz/splitmind && \
echo "source /splitmind/gdbinit.py" >> ~/.gdbinit

RUN SUDO_FORCE_REMOVE=yes apt-get purge -y build-essential git cmake bison flex libzip-dev python3-dev pkg-config sudo && \
apt-get autoremove -y --purge

COPY splitmind.gdb /
COPY tmux.conf /root/.tmux.conf

RUN echo "set context-ghidra always" >> ~/.gdbinit && \
echo "alias gdb='gdb -q'" >> ~/.bashrc && \
echo "source /splitmind.gdb" >> ~/.gdbinit && \
echo "source /ttyfd.gdb" >> ~/.gdbinit
